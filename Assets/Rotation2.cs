﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.HDPipeline;
using UnityEngine.Rendering;

public class App : MonoBehaviour
{

    [SerializeField] Volume volume;

    void SetHdriRotation(float rot) { HDRISky sky; volume.profile.TryGet(out sky); sky.rotation.value = rot; }
}