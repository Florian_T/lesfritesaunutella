﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Script_Taille : MonoBehaviour
{
    public Vector3 TailleRef;
    public Slider TailleJoueur;

    void Start()
    {
        TailleRef = new Vector3(0f, 1.70f, 0f);
        
    }

    void Update()
    {

     gameObject.transform.position = new Vector3(transform.position.x, 0 + (TailleRef.y - TailleJoueur.value), transform.position.z);
     
    }

}
