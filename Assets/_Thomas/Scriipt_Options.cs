﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scriipt_Options : MonoBehaviour
{
    public GameObject SliderTaille;
    public bool EtatBouton = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(EtatBouton == true)
        {
            SliderTaille.SetActive(true);
        }

        if (EtatBouton == false)
        {
            SliderTaille.SetActive(false);
        }
    }

    private void OnMouseDown()
    {
        if (gameObject.tag == "Options")
        {
            if(EtatBouton == false)
            {
                EtatBouton = true;
            }

            if(EtatBouton == true)
            {
                EtatBouton = false;
            }
        }
    }
}
