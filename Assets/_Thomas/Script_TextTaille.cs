﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Script_TextTaille : MonoBehaviour
{
    public static Text SliderValue;

    void Start()
    {
        SliderValue = GetComponent<Text>(); 
    }

    public void TextUpdate(float value)
    {
        SliderValue.text = Mathf.RoundToInt(value * 100) + "cm";
    }


}
