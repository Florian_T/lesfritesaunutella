﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Script_Start : MonoBehaviour
{
    Slider TailleJoueurValue;

    public void Start()
    {
        TailleJoueurValue = GetComponent<Script_Taille>().TailleJoueur;
    }

    public void ChangeScene()
    {
        PlayerPrefs.GetFloat("Taille");
        SceneManager.LoadScene("Thomas");
    }
}
