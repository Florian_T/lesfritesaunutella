﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongueVueScript : MonoBehaviour
{
    public float turnSpeed = 50f;
    public float maxUp = 50;
    public float maxDown = -50;
    public float currentHeight;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("p") & currentHeight > maxDown)
        {
            transform.Rotate(Vector3.right, -turnSpeed * Time.deltaTime);
            currentHeight = currentHeight - 1;
        }

        if (Input.GetKey("o") & currentHeight < maxUp)
        {
            transform.Rotate(Vector3.left, -turnSpeed * Time.deltaTime);
            currentHeight = currentHeight + 1;
        }
    }
}
