﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenduleSpeed : MonoBehaviour
{
    public Valve.VR.InteractionSystem.CircularDrive speedModifier;
    public float vitesseAnim = 1;
    public Animator pendule;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        vitesseAnim = speedModifier.outAngle;
        if (speedModifier.outAngle < 2)
        {
            vitesseAnim = 1f;
        }
        pendule.speed = vitesseAnim;
    }
}
