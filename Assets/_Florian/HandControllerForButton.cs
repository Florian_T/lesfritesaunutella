﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class HandControllerForButton : MonoBehaviour
{
    public GameObject _pickup; //Objet en collision
    public SteamVR_Action_Boolean grabAction = null;
    private SteamVR_Behaviour_Pose m_Pose = null;

    private void Start()
    {
        m_Pose = GetComponent<SteamVR_Behaviour_Pose>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Je rentre en contacte avec: " + other.name);
        if (other.tag == "Button")
        {
            Debug.Log("Button");
            _pickup = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Je sors");
        if (_pickup = other.gameObject)
        {
            _pickup = null;
        }
    }

    private void Update()
    {
        if (_pickup != null && grabAction.GetStateDown(m_Pose.inputSource))
        {
            Debug.Log("je détecte une pression");
            _pickup.GetComponent<ButtonMenuController>().Action();
        }
    }
}
