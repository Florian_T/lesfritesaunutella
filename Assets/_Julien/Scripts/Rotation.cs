﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.HDPipeline;
using UnityEngine.Rendering;

public class Rotation : MonoBehaviour
{
    public float vitesserota;
    public Valve.VR.InteractionSystem.CircularDrive speedModifier;
    public HDRISky sky;
    [SerializeField] Volume volume;

    // Start is called before the first frame update
    void Start()
    {
        volume.profile.TryGet(out sky);
        //volume.GetComponent<HDRISky>().rotation.value.
        transform.Rotate(0.0f, 0.0f, 45.0f);
    }

    // Update is called once per frame
    void Update()
    {
        //volume.profile.TryGet(out sky);
        //RenderSettings.skybox.SetFloat("_Rotation", vitesserota);
            
            vitesserota = speedModifier.outAngle;
            if(speedModifier.outAngle < 2)
            {
                 vitesserota = 0.1f;
            }
            transform.Rotate(Vector3.up * Time.deltaTime * vitesserota);
            if(sky.rotation.value == 360)
            {
                sky.rotation.value = 0;
            }
            sky.rotation.value = sky.rotation.value + Time.deltaTime * vitesserota/*(Time.deltaTime * vitesserota * 100)*/;



    }
}


//[SerializeField] Volume volume;

//void SetHdriRotation(float rot) { HDRISky sky; volume.profile.TryGet(out sky); sky.rotation.value = rot; } }

