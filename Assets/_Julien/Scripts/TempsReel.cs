using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

public class TempsReel : MonoBehaviour
{
    public float Heures;
    public float Minutes;
    public GameObject AiguilleMinutes;
    public GameObject AiguilleHeures;
    public SliderCircle aiguilleControle;
    float timeRunning = 0.0f;
    //public GameObject Midi;
    public Valve.VR.InteractionSystem.CircularDrive speedModifier;
    public AudioSource TicTac;
    bool heurePasse = false ;


    // Update is called once per frame
    void Update()
    {
        // * 60 = test rapide ,   *0.1f =  temps réel sans horloge PC
        if (!aiguilleControle.isDragging)
        {
            
            if (speedModifier.outAngle > 1)
            {
                timeRunning += Time.deltaTime *(speedModifier.outAngle * 100);
                TicTac.pitch = speedModifier.outAngle * 0.10f;
                if (TicTac.pitch < 1)
                {
                    TicTac.pitch = 1;
                }
            }
            else
            {
                timeRunning += Time.deltaTime;

            }
            Time.timeScale = 1.0f;
            Heures = timeRunning * 0.0083f;
            Minutes = aiguilleControle.CurrentValue + timeRunning * 0.1f;
        }
        else
        {
            Time.timeScale = 0.0f;
            timeRunning = 0.0f;/*+ aiguilleControle.CurrentValue;*/
            Minutes = aiguilleControle.CurrentValue;
        }

        //if ((AiguilleMinutes.transform.localRotation.z <= 0.1f && AiguilleMinutes.transform.localRotation.z >= - 0.1f ) && heurePasse == false)
        //{
        //    Debug.Log("heure ++");
        //    heurePasse = true;
        //    majHeuresSup();
        //    /*AiguilleMinutes.transform.localRotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, 0);*/
        //}
        //if (AiguilleMinutes.transform.localRotation.z >= 0.1f || AiguilleMinutes.transform.localRotation.z <= - 0.1f)
        //{
        //    Debug.Log("reset heure passé");
        //    heurePasse = false;
        //}
        AiguilleMinutes.transform.localRotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, Minutes);
        AiguilleHeures.transform.localRotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, Heures);
        //Debug.Log(AiguilleMinutes.transform.rotation.z);
    }

    private void Start()
    {
        //AiguilleMinutes.gameObject.GetComponent<Collision>();
        //Midi.gameObject.GetComponent<Collision>();
        
    }

    //private void OnTriggerEnter(Collider midi)
    //{
    //    Debug.Log("Enter"+ midi.name);
    //    if(midi.gameObject.name == "Midi")
    //    {
    //        majHeuresSup();
    //        Debug.Log("Heure Plus");
    //    }
    //}
    public void majHeuresSup()
    {
        Heures = Heures + 30;

    }

    public void majHeuresMoins()
    {
        Heures = Heures - 30;
    }
    
}
