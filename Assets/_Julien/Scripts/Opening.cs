﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Opening : MonoBehaviour
{
    private float open;
    public bool keyTurned = false;
    AudioSource OuvertureS;
    // Start is called before the first frame update
    void Start()
    {
        OuvertureS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (keyTurned)
        {
            open += Time.deltaTime *10;
            this.gameObject.transform.localRotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, -open);
            
        }
    }


    public void keyIsOkay()
    {
        keyTurned = true;
        OuvertureS.Play(0);
    }
}
